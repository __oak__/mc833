/*
* Code from http://www.unpbook.com/
* 
* 
*/

#include <sys/socket.h>
#include <netinet/in.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/select.h>
#include <string.h>

#define LISTENQ 5
#define MAXLINE 64
#define SERV_PORT 31472 

int
main(int argc, char **argv)
{
    int listenfd, connfd, udpfd, len, nready;
    ssize_t n;
    fd_set rset, allset;
    char buf[MAXLINE];
    socklen_t clilen;
    struct sockaddr_in cliaddr, servaddr, udpaddr;

    len = sizeof(struct sockaddr_in);
    
    if ((listenfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        perror("socket error");
        return 1;
    }
    
    bzero(&servaddr, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port        = htons(SERV_PORT);
    udpaddr = servaddr;
    
    if (bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0)  {
        perror("bind error bind tcp");
        close(listenfd);
        return 1;
    }
    
    if (listen(listenfd, LISTENQ) != 0) {
        perror("listen error listen tcp");
        close(listenfd);
        return 1;
    }

    /* Bind udp socket */
    if ((udpfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0){

        perror("listen error create udp");
        close(udpfd);
        close(listenfd);
        return 1;
    }
    if(bind(udpfd, (struct sockaddr *) &udpaddr, sizeof(udpaddr)) < 0){
        perror("listen error bind");
        close(udpfd);
        close(listenfd);
        return 1;
    }


    int optval = 1;
    setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));

    printf("%d and %d\n", listenfd, udpfd);
    FD_ZERO(&allset);
    FD_SET(listenfd, &allset);
    FD_SET(udpfd, &allset);

    for ( ; ; ) {
        rset = allset;          /* structure assignment */
        nready = select(udpfd+1, &rset, NULL, NULL, NULL);
        
        if (FD_ISSET(listenfd, &rset)) {        /* new client connection */
            clilen = sizeof(cliaddr);
            connfd = accept(listenfd, (struct sockaddr *) &cliaddr, &clilen);
            if (!fork()){
                close(listenfd);
                close(udpfd);
                while((n = recv(connfd, buf, MAXLINE, 0))>0){
                    buf[n] = 0;
                    char *a = (char *)&(cliaddr.sin_addr.s_addr);
                    printf("Printing information for %hhu.%hhu.%hhu.%hhu:%u\n",
                           a[0], a[1], a[2], a[3], ntohs(cliaddr.sin_port));
                        printf("%s",buf);
                    send(connfd, buf, n, 0);
                }
                close(connfd);
                exit(1);
            }else{
                close(connfd);
            }
        }

        if (FD_ISSET(udpfd, &rset)) {        /* new client connection */
            printf("it is set\n");
            if ((n = recvfrom(udpfd,
                              buf,
                              sizeof(buf),
                              0,
                              (struct sockaddr*)&cliaddr,
                              &len)) > 0){
                char *a = (char *)&(cliaddr.sin_addr.s_addr);
                printf("Printing information for %hhu.%hhu.%hhu.%hhu:%u\n",
                       a[0], a[1], a[2], a[3], ntohs(cliaddr.sin_port));
                buf[n] = 0;
                printf("%s\n", buf);
                sendto(udpfd, buf, n, 0, (struct sockaddr*)&cliaddr, len);
            }
        }
    }
}
