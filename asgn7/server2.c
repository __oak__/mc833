#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <poll.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#define SERVER_PORT 31472
#define MAX_PENDING 5
#define MAX_LINE 256

int main()
{
	struct sockaddr_in sin, udpfd;
	char buf[MAX_LINE];
	int len;
	int s, new_s;

	/* build address data structure */
	bzero((char *)&sin, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = INADDR_ANY;
	sin.sin_port = htons(SERVER_PORT);

	/* setup passive open */
	if ((s = socket(PF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0)) < 0) {
		perror("simplex-talk: socket");
		exit(1);
	}
	if ((bind(s, (struct sockaddr *)&sin, sizeof(sin))) < 0) {
		perror("simplex-talk: bind");
		exit(1);
	}
	listen(s, MAX_PENDING);

#define POOL_SIZE 100
    int socket_list[POOL_SIZE];
    struct sockaddr_in addresses[POOL_SIZE] = {0};
    int sock_counter = 0;
    
	/* wait for connection, then receive and print text */
    char *a = (char *)&(sin.sin_addr.s_addr);
    printf("Starting on %hhu.%hhu.%hhu.%hhu:%u\n",
                 a[0],
                 a[1],
                 a[2],
                 a[3],
                 htons(sin.sin_port));

	while(1) {
        len = sizeof(struct sockaddr_in);
		if ((new_s = accept4(s,
                                                 (struct sockaddr *)&addresses[sock_counter],
                                                 &len,
                                                 SOCK_NONBLOCK)) < 0){
            if (errno != EWOULDBLOCK){
                printf("%d\n",errno);
                printf("%d\n",EWOULDBLOCK);
                perror("simplex-talk: accept");
                exit(1);
            }
        }else{
            socket_list[sock_counter++] = new_s;
        }
        for (int i = 0; i < sock_counter; i++){
            char *a = (char *)&(addresses[i].sin_addr.s_addr);
            while ((len = recv(socket_list[i], buf, sizeof(buf), 0)) > 0){
                printf("Printing information for %hhu.%hhu.%hhu.%hhu:%u\n",
                             a[0],
                             a[1],
                             a[2],
                             a[3],
                             ntohs(addresses[i].sin_port));
                struct sockaddr_in *addr = (struct sockaddr_in*) &addresses[i];
                send(socket_list[i],buf,len,0);
                fputs(buf, stdout);
            }
        }
        if (FD_ISSET(udpfd, &rset)) {        /* new client connection */
            if ((n = recvfrom(udpfd,
                              buf,
                              sizeof(buf),
                              0,
                              (struct sockaddr*)&cliaddr,
                              &len)) > 0){
                char *a = (char *)&(cliaddr.sin_addr.s_addr);
                printf("Printing information for %hhu.%hhu.%hhu.%hhu:%u\n",
                       a[0], a[1], a[2], a[3], ntohs(cliaddr.sin_port));
                buf[n] = 0;
                printf("%s\n", buf);
                sendto(udpfd, buf, n, 0, (struct sockaddr*)&cliaddr, len);
            }
        }
	}
}
