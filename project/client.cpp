#include "utils.hpp"

struct game {
    char board[3][3];
    char play;
};

Session session;
DH *session_keys;
unordered_map<string, string> keyset;
unordered_map<string, struct game> games;
/* map ids to users */
unordered_map<int, string> confirmations;
mutex confirmation_mutex;

// This makes sense to be global
string username;

string get_peer_secret(string peer){
    if (keyset.count(peer)){
        return compute_shared_secret(session_keys, keyset[peer]);
    }
    auto proto_message = make_shared<Message>();
    proto_message->set_to(peer);
    proto_message->set_from(username);
    proto_message->set_msg(serialize_pubkey(session_keys));
    proto_message->set_cmd(Message::get_key);
    session.safe_send(proto_message);
    while(!keyset.count(peer)){usleep(10e3);};
    return compute_shared_secret(session_keys, keyset[peer]);
}

int connect_to_host(string host, int port){
    struct hostent *hp = gethostbyname(host.c_str());
    ensure(hp, "Failed to resolve hostname");
    struct sockaddr_in sin = {
        .sin_family = AF_INET,
        .sin_port = htons(port),
        //dirty cast
        .sin_addr.s_addr = *(in_addr_t*)hp->h_addr
    };
    int sock = socket(PF_INET, SOCK_STREAM, 0);
    ensure(sock > 0, "Failure to create socket");
    ensure(!connect(sock, (struct sockaddr *)&sin, sizeof(sin)),
           "Failed to connect");
    return sock;
}

void print_board(struct game &game){
    cout << "    A   B   C" << endl;
    for (int i = 0; i < 9; i++){
        if (i%3 == 0) cout << i/3;
        cout << " | " << (game.board[i/3][i%3] ? game.board[i/3][i%3] : ' ');
        if (i%3 == 2) cout << " |" << endl;
    }
}

/* This function is used for operations that might depend on peer key.
 * Since we might need to receive the peer key to continue sending the message,
 * we do that asynchronously in a separate thread. */
void decrypt_with_callback(shared_ptr<Message> proto_msg, 
                           void (*callback)(string&, shared_ptr<Message>)){
    string msg = proto_msg->msg();
    string peer_secret = get_peer_secret(proto_msg->from());
    decrypt((unsigned char *)msg.c_str(),
            msg.size(),
            (unsigned char *)peer_secret.c_str());
    int padding_size = (int)msg[msg.size()-1];
    msg.resize(msg.size()-padding_size);
    if (debug) cerr << "found padding of " << padding_size << endl;
    callback(msg, proto_msg);
}

/* Functions used for callback on recv_and_draw. */

void print_message(string &contents, shared_ptr<Message> sender_msg){
    cout << sender_msg->from()<< ": " << contents<< endl;
}

void write_file(string &contents, shared_ptr<Message> sender_msg){
    ofstream outfile("/var/redes/" + sender_msg->filename());
    if (debug) cerr << "Writing to the file: /var/redes/"
                    << sender_msg->filename();
    outfile.write(contents.c_str(), contents.size());
    outfile.close();
}

void get_oponent_play(string &contents, shared_ptr<Message> sender_msg){
    int row = (int)contents[0] - (int)'0',
        column = (int)contents[1] - (int)'A';
    // Just ignore wrong game play
    if (row > 3 || row < 0 || column > 3 || row < 0) return;
    struct game g;
    if (sender_msg->cmd() == Message::game){
        g = {};
        g.play = 'o';
    }else{
        g = games[sender_msg->from()];
    }
    g.board[row][column] = g.play == 'o' ? 'x' : 'o';
    games[sender_msg->from()] = g;
    print_board(g);
}

void recv_and_draw(){
    for (;;){
        auto root = session.safe_read();
        if (debug) cerr << ">> Receiving " << root->cmd() << endl;
        if (root->cmd() == Message::who){
            for(int i = 0; i < root->users_size(); i++){
                const auto& user = root->users(i);
                cout << user.name() << " status: " << user.online() << endl;
            }
        // Someone asked for confirmation
        }else if (root->cmd() == Message::ask_confirm){
            cout << root->msg() << endl;
            cout << "Type 'accept " << root->id() << "' to accept." << endl;
            lock_guard<mutex> g(confirmation_mutex);
            confirmations[root->id()] = root->from();
        // Received confirmation
        }else if (root->cmd() == Message::res_confirm){
            cerr << "Received confirmation for: " << root->id() << endl;
            lock_guard<mutex> g(confirmation_mutex);
            confirmations[root->id()] = root->from();
        }else if (root->cmd() == Message::game ||
                  root->cmd() == Message::gamea){
            thread t(decrypt_with_callback, root, get_oponent_play);
            t.detach();
        }else if (root->cmd() == Message::sendf){
            thread t(decrypt_with_callback, root, write_file);
            t.detach();
        }else if (root->cmd() == Message::send_key){
            keyset[root->from()] = root->msg();
        }else if (root->cmd() == Message::get_key){
            keyset[root->from()] = root->msg();
            auto proto_message = make_shared<Message>();
            proto_message->set_to(root->from());
            proto_message->set_from(root->to());
            if (debug)
                cerr << "Asked for key " << root->from() << " "
                     << root->to() << endl;
            proto_message->set_msg(serialize_pubkey(session_keys));
            proto_message->set_cmd(Message::send_key);
            session.safe_send(proto_message);
        }else if (root->cmd() == Message::send){
            thread t(decrypt_with_callback, root, print_message);
            t.detach();
        }else{
            string msg = root->msg();
            string from = root->has_from() ? root->from() : "" ;
            cout << from << ": " << msg << endl;
        }
        usleep(10e3);
    }
}

void register_user(){
    auto register_entry = make_shared<Message>();
    register_entry->set_cmd(Message::registration);
    register_entry->set_from(username);

    if (debug) cerr << "Finished generating keys" << endl;
    register_entry->set_msg(serialize_pubkey(session_keys));
    session.safe_send(register_entry);

    // get response from server with his pk
    auto server_pk = session.safe_read();
    session.secret = compute_shared_secret(session_keys, server_pk->msg());
    session.setup_encryption();
}

/* Implement confirmation before sending proto_msg. */
void send_with_confirmation(shared_ptr<Message> proto_msg){
    auto conf_msg = make_shared<Message>();
    int id;
    get_rand(&id, sizeof(int));
    conf_msg->set_id(id);
    conf_msg->set_from(proto_msg->from());
    conf_msg->set_to(proto_msg->to());
    conf_msg->set_cmd(Message::ask_confirm);
    if (proto_msg->cmd() == Message::gamea){
        session.safe_send(proto_msg);
        return;
    }
    if (proto_msg->cmd() == Message::sendf){
        conf_msg->set_msg("Would you like to receive file "+
                           proto_msg->filename()+
                           " from " + proto_msg->from());
    }else{ // Can only be game
        conf_msg->set_msg("Would you like to start a game with " +
                           proto_msg->from() + "?");
    }
    session.safe_send(conf_msg);
    // have the usleep on the while so g can go on and off the scope
    while(true){
        confirmation_mutex.lock();
        if (confirmations.count(id) > 0){
            confirmations.erase(id);
            confirmation_mutex.unlock();
            break;
        }
        confirmation_mutex.unlock();
        usleep(10e3);
    }
    session.safe_send(proto_msg);
}

/* This function is similat to decrypt_with_callback, except it does encryption
 * and paddding checking. Use shared_ptr msg here for performance. */
void encrypt_and_send(string msg,
                      shared_ptr<Message> proto_msg,
                      void (*callback)(shared_ptr<Message>)){
    int size_before_pad = msg.size();
    msg.resize(size_after_pad(size_before_pad));
    string peer_secret = get_peer_secret(proto_msg->to());
    encrypt((unsigned char *)msg.c_str(),
             size_before_pad, 
             (unsigned char *)peer_secret.c_str());
    proto_msg->set_msg(msg);
    callback(proto_msg);
}

/* Simple wrapper for callback use */
void simple_send(shared_ptr<Message> msg){
    session.safe_send(msg);
}

int main(int argc, char *argv[]){
    if (argc != 4){
        perror("call ./server <host> <port> <username>");
        exit(1);
    }
    username = argv[3];
    string host = argv[1];
    int port = atoi(argv[2]);
    session.sock = connect_to_host(host, port);

    // Get session key from file or generate them if the file doesn't exist
    session_keys = generate_session_keys(username);

    register_user();
    //Start sending client name
    thread drawer(recv_and_draw);
    drawer.detach();
    try{
    for (;;){
        string cmd;
        cin >> cmd;
        // always get an id
        int id;
        get_rand(&id, sizeof(int));
        auto proto_message = make_shared<Message>();
        transform(cmd.begin(), cmd.end(), cmd.begin(), ::tolower);
        proto_message->set_id(id);
        proto_message->set_from(username);
        if (cmd == "send" || cmd == "sendg"){
            string peer_name, msg;
            cin >> peer_name;
            getline(cin, msg);

            if (debug) cerr << "Send cmd" << endl;
            proto_message->set_to(peer_name);
            proto_message->set_cmd(cmd == "send" ?
                                   Message::send :
                                   Message::sendg);
            if (cmd == "send"){
                proto_message->set_cmd(Message::send);
                thread t(encrypt_and_send, msg, proto_message, simple_send);
                t.detach();
            }else{
                proto_message->set_cmd(Message::sendg);
                proto_message->set_msg(msg);
                session.safe_send(proto_message);
            }
        }else if (cmd == "accept"){
            cin >> id;
            lock_guard<mutex> g(confirmation_mutex);
            if (!confirmations.count(id)){
                cerr << "Invalid id." << endl;
                continue;
            }
            string peer = confirmations[id];
            confirmations.erase(id);
            proto_message->set_cmd(Message::res_confirm);
            proto_message->set_id(id);
            proto_message->set_to(peer);
            session.safe_send(proto_message);
        }else if (cmd == "game" || cmd == "gamea"){
            string peer, column, row;
            cin >> peer >> column >> row;
            struct game g;
            if (cmd == "game"){
                g = {};
                g.play = 'x';
                proto_message->set_cmd(Message::game);
            }else{
                g = games[peer];
                proto_message->set_cmd(Message::gamea);
            }
            g.board[stoi(row)][((int)column[0])-((int)'A')] = g.play;
            games[peer] = g;
            proto_message->set_to(peer);
            print_board(g);
            thread t(encrypt_and_send,
                     row + column,
                     proto_message,
                     send_with_confirmation);
            t.detach();
        }else if (cmd == "createg" || cmd == "joing"){
            string group_name;
            cin >> group_name;
            proto_message->set_msg(group_name);
            proto_message->set_cmd(cmd == "createg" ?
                                   Message::createg :
                                   Message::joing);
            session.safe_send(proto_message);
        }else if (cmd == "sendf"){
            string peer_name, path;
            cin >> peer_name >> path;
            
            /* File operations */
            ifstream file(path, ios::binary);
            if (!file){
                cerr << "Unable to open send file" << endl;
                continue;
            }
            int filesize = file_size(path);
            if (filesize> MAX_FILE_SIZE){
                cerr << "File bigger than maximum allowed" << endl;
                continue;
            }
            /* Read file */
            char *data = new char[filesize];
            file.read(data, filesize);
            file.close();
            string s(data, filesize);

            // find file name
            int last_slash = path.find_last_of("/");
            if (debug) cerr << "File name: " << proto_message->from() << endl;

            proto_message->set_filename(path.substr(last_slash?
                                                    last_slash+1:
                                                    last_slash));
            proto_message->set_to(peer_name);
            proto_message->set_id(id);
            proto_message->set_cmd(Message::sendf);
            thread t(encrypt_and_send, s, proto_message, send_with_confirmation);
            t.detach();
            delete[] data;
        }else if (cmd == "exit"){
            proto_message->set_cmd(Message::exit);
            session.safe_send(proto_message);
        }else if (cmd == "who"){
            proto_message->set_cmd(Message::who);
            session.safe_send(proto_message);
        } else {
            // if (debug) cerr << "Invalid command" << endl;
            continue;
        }
        //no-op for WHO and EXIT and whatever else
        if (cmd == "exit") break;
    }
    } catch (...) {
        cerr << "Error" << endl;
    }
}
