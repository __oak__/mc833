#include "utils.hpp"

struct userinfo {
    struct sockaddr_in addr;
    string name;
    int fd;
    queue<shared_ptr<Message>> msgqueue;
    mutex msgq_lock, chatroom_lock;
    unordered_set<string> chatrooms;
    Session session;
};

unordered_map<string, struct userinfo> Users;
//unordered_map<string, unordered_set<string> groups;
mutex user_mutex;

const int MAX_CLIENTS = 100;

int setup_listening_socket(int port){
    struct sockaddr_in sin = {
        .sin_family = AF_INET,
        .sin_port = htons(port),
        .sin_addr.s_addr = INADDR_ANY
    };
    int sock = socket(PF_INET, SOCK_STREAM, 0), optval = 1;
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval));
    ensure(sock > 0, "Failure to create socket");
    ensure(!bind(sock, (struct sockaddr *)&sin, sizeof(sin)),
           "Failed to bind socket");
    ensure(!listen(sock, MAX_CLIENTS), "Failed to listen");
    return sock;
}

void safe_push(struct userinfo &user, shared_ptr<Message> msg){
    if (debug) cerr << "queueing msg"  << endl;
    lock_guard<mutex> guard(user.msgq_lock);
    user.msgqueue.push(msg);
}

void send_print(struct userinfo &user, string s){
    auto m = make_shared<Message>();
    m->set_cmd(Message::print);
    m->set_msg(s);
    safe_push(user, m);
}

void send_info_to_user(string username){
    // grab another reference
    auto& info = Users[username];
    while(info.fd){
        if (!info.msgqueue.empty()){
            // guard access to msgqueue
            auto msg = info.msgqueue.front();
            // If we fail to deliver a message, tear down the send and recv
            // threads
            if (!info.session.safe_send(msg)){
                info.session.teardown();
                return;
            }
            info.msgqueue.pop();
            if (msg->cmd() == Message::send  ||
                msg->cmd() == Message::sendf ||
                msg->cmd() == Message::game  ||
                msg->cmd() == Message::gamea){
                auto& sender = Users[msg->from()];
                string response = info.name +
                                  " received msg " +
                                  to_string((unsigned int )msg->id());
                auto protomsg = make_shared<Message>();
                protomsg->set_cmd(Message::ack);
                protomsg->set_msg(response);
                // guard access to msgqueue
                safe_push(sender, protomsg);
            }
        }
        // sleep for 1msec before pooling again.
        usleep(10e3);
    }
}

DH *session_keys = generate_session_keys("server");

struct userinfo &register_user(int csock, struct sockaddr_in peer_addr){
    Session user_session(csock);
    auto user_data = user_session.safe_read();
    string username = user_data->from();
    // If another user logs with the same username, kick the old one
    if (debug) cerr << "registering " << username << endl;
    user_mutex.lock();
    bool exists_before = Users.count(username);
    auto& user = Users[username];
    user_mutex.unlock();
    user.addr = peer_addr;
    user.session = user_session;
    user.name = username;
    user.fd = csock;
    if (exists_before){
        lock_guard<mutex> guard(user.chatroom_lock);
        user.chatrooms.clear();
    }
    // Handle crypto

    // Send to the user my pk
    user.session.secret = compute_shared_secret(session_keys, user_data->msg());
    auto server_pk = make_shared<Message>();
    server_pk->set_cmd(Message::ack);
    server_pk->set_msg(serialize_pubkey(session_keys));
    user.session.safe_send(server_pk);
    // safe_push(user, server_pk);
    if (debug) cerr << "Finished key setup" << endl;
    user.session.setup_encryption();
    return user;
}

void handle_user(int csock, struct sockaddr_in peer_addr){
    struct userinfo &info = register_user(csock, peer_addr);
    thread info_sender(send_info_to_user, info.name);
    info_sender.detach();
    try {
    for (;;){
        shared_ptr<Message> root = info.session.safe_read();
        if (root->cmd() == Message::get_key ||
            root->cmd() == Message::send_key){
            // just relay the messages
            string user = root->to();
            if (Users.count(user)){
                auto& peer = Users[user];
                safe_push(peer, root);
            }else{
                send_print(info, user + " not found");
            }
        }else if (root->cmd() == Message::send  ||
                  root->cmd() == Message::sendf ||
                  root->cmd() == Message::game  ||
                  root->cmd() == Message::gamea){
            string user = root->to();
            int id = root->id();
            if (debug) cerr << "received message" << endl;
            if (Users.count(user)){
                auto& peer = Users[user];
                safe_push(peer, root);
                send_print(info, "mensagem " + to_string((unsigned)root->id()) +
                                 " enfileirada para " + user);
            }else{
                send_print(info, user + " not found");
            }
        }else if (root->cmd() == Message::createg){
            string name = root->msg();
            send_print(info, "grupo " + name + " criado com sucesso");
        }else if (root->cmd() == Message::joing){
            lock_guard<mutex> guard(info.chatroom_lock);
            string name = root->msg();
            info.chatrooms.insert(name);
            send_print(info, info.name + "entrou no grupo " + name);
        }else if (root->cmd() == Message::sendg){
            string chat = root->to(), msg = root->msg();
            int id = root->id();
            root->set_from("<" + chat + "> " + root->from());
            lock_guard<mutex> guard(user_mutex);
            for (auto& userpair: Users){
                auto& user = userpair.second;
                lock_guard<mutex> guard2(user.chatroom_lock);
                if (user.chatrooms.count(chat) && user.name != info.name){
                    safe_push(user, root);
                }
            }
            send_print(info, "mensagem " + to_string(id) +
                             "enfileirada para " + chat);
        }else if (root->cmd() == Message::who){
            auto msglist = make_shared<Message>();
            msglist->set_cmd(Message::who);
            lock_guard<mutex> guard(user_mutex);
            for(auto& userpair: Users){
                auto& user = userpair.second;
                auto* entry = msglist->add_users();
                entry->set_name(user.name);
                entry->set_online(!!user.fd);
            }
            safe_push(info, msglist);
            if (debug) cerr << "Done whoing" << endl;
        // Should only be hit by exit and erroneus commands
        }else if (root->cmd() == Message::ask_confirm ||
                  root->cmd() == Message::res_confirm){
            auto& peer = Users[root->to()];
            safe_push(peer, root);
        }else{
            info.fd = 0;
            break;
        }
    }
    } catch(...) {
        info.fd = 0;
        if (debug) cerr << "Invalid message " << endl;
    }
}

int main(int argc, char *argv[]){
    if (argc != 2){
        perror("call ./server <port>");
        exit(1);
    }
    int port = atoi(argv[1]);
    int lsock = setup_listening_socket(port);
    if (debug) cerr << "Starting server" << endl;
    for(;;){
        struct sockaddr_in peer_addr;
        socklen_t socklen = sizeof(struct sockaddr_in);
        int csock = accept(lsock, (struct sockaddr *)&peer_addr, &socklen);
        thread t(handle_user, csock, peer_addr);
        t.detach();
    }
    close(lsock);
}
