protoc --cpp_out=. protocol.proto
debug=false
compile="clang++ -Ddebug=$debug -g -lprotobuf -lcrypto -lpthread -std=c++11 protocol.pb.cc"
for i in server client; do
  $compile $i.cpp -o $i
done
