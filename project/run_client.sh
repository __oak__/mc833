#/bin/bash
host=$1
port=$2
user=$3

fifo=output-$user

[ -e $fifo ] && mkfifo $fifo
tmux new-session -d -s $user "./client $host $port $user &> $fifo"\; split-window -d "tail -f -n1 $fifo" \; attach
[ -e $fifo ] && rm $fifo
