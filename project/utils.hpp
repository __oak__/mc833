#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <poll.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#include "protocol.pb.h"
#include <openssl/evp.h>
#include <openssl/bn.h>
#include <openssl/dh.h>
#include <iostream>
#include <fstream>
#include <iterator>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <mutex>
#include <thread>
#include <iostream>
#include <queue>
#include <cstdint>
#include <random>
#include <algorithm>
#include "dhparam.h"

using namespace std;

const int MAX_FILE_SIZE = 66*1000*1000;
const int BUF_SIZE = (1<<15);
const int BLOCK_SIZE = 16;

void hex_print(string s){
    for(auto &e: s){
        fprintf(stderr,"%hhx",e);
    }
    cerr << endl;
}

inline int size_after_pad(int size){
    return BLOCK_SIZE*(size/BLOCK_SIZE) + (!!size%BLOCK_SIZE)*BLOCK_SIZE;
}

void ensure(bool condition, string msg){
    if (!condition){
        cerr << msg << endl;;
        throw msg;
    }
}

inline bool exist_file(const string& name) {
    return ( access( name.c_str(), F_OK ) != -1 );
}


void get_DH_key_from_file(string &username, DH * dh_key){
    /* Oversize the buffer for now */
    string keybytes;
    ifstream key_file(username + ".privkey");
    auto ctx = BN_CTX_new();
    string str((istreambuf_iterator<char>(key_file)),
               istreambuf_iterator<char>());
    dh_key->priv_key = BN_bin2bn((const unsigned char *)str.c_str(),
                                 str.size(),
                                 0);
    dh_key->pub_key = BN_new();
    BN_mod_exp(dh_key->pub_key,
               dh_key->g,
               dh_key->priv_key,
               dh_key->p,
               ctx);
    BN_CTX_free(ctx);
}

void save_privkey(string &username, DH* dh_key){
    /* Oversize the buffer for now */
    char *buf = new char [BN_num_bytes(dh_key->priv_key)];
    BN_bn2bin(dh_key->priv_key, (unsigned char *)buf);
    string keybytes;
    ofstream key_file(username+".privkey");
    key_file.write(buf, BN_num_bytes(dh_key->priv_key));
}

DH* generate_session_keys(string username) {
    DH *session_keys = get_dh512();
    ensure(session_keys, "Could not initialize DH key");
    // just abort if there are any errors
    int errors = 0;
    ensure(DH_check(session_keys, &errors), "Check could not be performed");
    ensure(!errors, "Invalid DH parameters");
    if (exist_file(username + ".privkey")){
        get_DH_key_from_file(username, session_keys);
    }else{
        ensure(DH_generate_key(session_keys), "Could not generate key");
        save_privkey(username, session_keys);
    }
    return session_keys;
}

string serialize_pubkey(DH *session_keys){
    int repr_size = BN_num_bytes(session_keys->pub_key);
    string binpk;
    binpk.resize(repr_size);
    BN_bn2bin(session_keys->pub_key, (unsigned char*) &binpk[0]);
    return binpk;
}

string compute_shared_secret(DH *session_keys, string bin_user_pk){
    BIGNUM *user_pubkey = BN_new();
    BN_bin2bn((unsigned char *)&bin_user_pk[0],
              bin_user_pk.size(),
              user_pubkey);
    string secret;
    secret.resize(DH_size(session_keys));
    ensure(0 < DH_compute_key((unsigned char *) &secret[0],
                              user_pubkey,
                              session_keys),
           "Failed to compute DH shared key");
    BN_free(user_pubkey);
    return secret;
}

void get_rand(void *buf, int size){
    ifstream random_file("/dev/urandom", ios::binary);
    ensure(!!random_file, "Could not open /dev/urandom");
    random_file.read((char *)buf, size);
}

void encrypt(unsigned char *data, int size, unsigned char *key){
    int padding, outlen;
    EVP_CIPHER_CTX context;
    ensure(EVP_EncryptInit(&context, EVP_aes_128_ecb(), key, 0),
           "Init failure");
    ensure(EVP_EncryptUpdate(&context, data, &outlen, data, size),
            "Update failure");
    ensure(EVP_EncryptFinal(&context, data+outlen, &padding),
           "Decryption failure");
}

void decrypt(unsigned char *data, int size, unsigned char *key){
    int padding, outlen;
    EVP_CIPHER_CTX context;
    ensure(EVP_DecryptInit(&context, EVP_aes_128_ecb(), key, 0),
           "Init failure");
    ensure(EVP_DecryptUpdate(&context, data, &outlen, data, size),
            "Update failure");
    ensure(EVP_DecryptFinal(&context, data+outlen, &padding),
           "Decryption failure");
}

static inline int min(const int a, const int b){
    return a > b ? a : b;
}

class Session{
public:
    int sock;
    // key = secret[-4:]
    string secret;
    unsigned char key[2*BLOCK_SIZE];
    bool use_encryption;
    Session(int sock) : sock(sock), use_encryption(false) { }; 
    Session() : use_encryption(false) { }; 

    void setup_encryption(){
        ensure(secret.size() >= 2*BLOCK_SIZE,
               "Size of shared secret must be at least key size.");
        memcpy(key,
               secret.c_str(),
               2*BLOCK_SIZE);
        if (debug) hex_print(secret);
        use_encryption = true;
    }

    bool encrypt_and_send(char *data, int size) {
        int status;
        if (use_encryption){
            // leave space for the last block padding
            int outlen = size_after_pad(size);
            // encrypt to the same buffer
            encrypt((unsigned char *)data+BLOCK_SIZE, size, key);
            // size of the data
            ((int*)data)[0] = outlen;
            ((int*)data)[1] = size;
            encrypt((unsigned char *)data, 2*sizeof(int), key);
            status = send(sock, data, outlen+BLOCK_SIZE, 0);
        }else{ // For key negociation
            // size of the data
            ((int*)data)[0] = ((int*)data)[1] = size;
            // account for size block
            status = send(sock, data, size+BLOCK_SIZE, 0);
        }
        if (debug) cerr << "Sent " << status << " bytes." << endl;
        return status > 0;
    }

    void recv_and_decrypt(char *buf, int size){
        // the smallest piece of information is a block
        ensure(size >= BLOCK_SIZE,
               "No sense in receiving something smaller than BLOCK_SIZE");
        int bytes_read = 0;
        int padding, outlen;
        EVP_CIPHER_CTX context;
        if (use_encryption)
            ensure(EVP_DecryptInit(&context, EVP_aes_128_ecb(), key, 0),
                   "Init failure");
        for(int i = 0; i < size; i += BUF_SIZE){
            int round_size = recv(sock,
                                  buf+i,
                                  size - i > BUF_SIZE ? BUF_SIZE : size - i,
                                  MSG_WAITALL);
            ensure(round_size >= 0,
                   "Read failed, tearing down the connection.");
            if(use_encryption)
                ensure(EVP_DecryptUpdate(&context,
                                         (unsigned char*)buf+i,
                                         &outlen,
                                         (unsigned char*)buf+i,
                                         round_size),
                    "Update failure");
            bytes_read += round_size;
        }
        if(use_encryption)
            ensure(EVP_DecryptFinal(&context,
                        (unsigned char*)buf+outlen, &padding),
                   "Decryption failure");
        ensure(bytes_read == size, "Failed to read all the contents");
    }

    bool safe_send(shared_ptr<Message> data){
        int datasize = data->ByteSize();
        if (debug) cerr << "Sending :" << datasize << endl;
        // leave space for padding
        char *buf = new char [datasize+2*BLOCK_SIZE];
        data->SerializeToArray(buf+BLOCK_SIZE, datasize);
        // Zero the first block, since it's not going to be completely filled
        // for (int i = 0; i < BLOCK_SIZE; i++) buf[i] = 0;
        bool status = encrypt_and_send(buf, datasize);
        delete[] buf;
        if (debug) cerr << "Status of safe_send: " << status << endl;
        return status;
    }

    shared_ptr<Message> safe_read(){
        /* Metadata containing the file size */
        int sizebuf[BLOCK_SIZE/sizeof(int)];
        recv_and_decrypt((char *)sizebuf, sizeof(sizebuf));
        int readsize = sizebuf[0];
        int after_decryption = sizebuf[1];

        /* Receive the file */
        // limit in 10k for now
        if (debug)
            cerr << "Readsize " << readsize
                 << " after_decryption " << after_decryption << endl; 
        ensure(readsize < MAX_FILE_SIZE, "File too large");
        char *buf = new char[readsize+1];
        recv_and_decrypt(buf, readsize);

        auto m = make_shared<Message>();
        if (debug){
            cerr << "Pad "<< (int)buf[readsize] << endl;
            cerr << "Pad "<< (int)buf[readsize-1] << endl;
        }
        m->ParseFromArray(buf, after_decryption);
        if(debug) cerr << m->DebugString() << endl;
        delete[] buf;
        return m;
    }

    void teardown(){
        close(sock);
    }
};

int file_size(string path){
    ifstream file(path, ios::binary | ios::ate);
    int size = file.tellg();
    file.close();
    return size;
}
