#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <poll.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>

#define SERVER_PORT 31472
#define MAX_LINE 256

int main(int argc, char * argv[])
{
    FILE *fp;
    struct hostent *hp;
    struct sockaddr_in sin, peeraddr;
    char *host;
    char buf[MAX_LINE];
    int s;
    int len;
    int peersize = sizeof(peeraddr);
    if (argc==2) {
        host = argv[1];
    } else {
        fprintf(stderr, "usage: ./client host\n");
        exit(1);
    }

    /* translate host name into peer’s IP address */
    hp = gethostbyname(host);
    if (!hp) {
        fprintf(stderr, "simplex-talk: unknown host: %s\n", host);
        exit(1);
    }

    /* build address data structure */
    bzero((char *)&sin, sizeof(sin));
    sin.sin_family = AF_INET;
    bcopy(hp->h_addr, (char *)&sin.sin_addr, hp->h_length);
    sin.sin_port = htons(SERVER_PORT);

    /* active open */
    if ((s = socket(PF_INET, SOCK_DGRAM | SOCK_NONBLOCK, 0)) < 0) {
        perror("simplex-talk: socket");
        exit(1);
    }

    char recvbuf[MAX_LINE];
    int socklen = sizeof(struct sockaddr_in);
    /* main loop: get and send lines of text */
    while (fgets(buf, sizeof(buf), stdin)) {
        // see if we have data to show
        buf[MAX_LINE-1] = '\0';
        len = strlen(buf) + 1;
        sendto(s, buf, len, 0, (struct sockaddr*)&sin, socklen);
        recvfrom(s, recvbuf, sizeof(recvbuf), 0, (struct sockaddr*)&peeraddr, &peersize);
        printf("%s",recvbuf);
    }
    close(s);
}
