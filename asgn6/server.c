#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <poll.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#define SERVER_PORT 31472
#define MAX_PENDING 5
#define MAX_LINE 256

int main()
{
    struct sockaddr_in sin;
    char buf[MAX_LINE];
    int len;
    int s, new_s;

    /* build address data structure */
    bzero((char *)&sin, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port = htons(SERVER_PORT);

    /* setup passive open */
    if ((s = socket(PF_INET, SOCK_DGRAM, 0)) < 0) {
        perror("simplex-talk: socket");
        exit(1);
    }
    if ((bind(s, (struct sockaddr *)&sin, sizeof(sin))) < 0) {
        perror("simplex-talk: bind");
        exit(1);
    }

    /* wait for connection, then receive and print text */
    char *a = (char *)&(sin.sin_addr.s_addr);
    printf("Starting on %hhu.%hhu.%hhu.%hhu:%u\n",
           a[0], a[1], a[2], a[3], htons(sin.sin_port));
    struct sockaddr_in peer_addr;
    int socklen = sizeof(struct sockaddr_in);
    while(1) {
        len = sizeof(struct sockaddr_in);
        int size;
        if ((size = recvfrom(s,
                             buf,
                             sizeof(buf),
                             0,
                             (struct sockaddr*)&peer_addr,
                             &len)) > 0){
            char *a = (char *)&(peer_addr.sin_addr.s_addr);
            printf("Printing information for %hhu.%hhu.%hhu.%hhu:%u\n",
                   a[0], a[1], a[2], a[3], ntohs(peer_addr.sin_port));
            sendto(s, buf, size, 0, (struct sockaddr*)&peer_addr, socklen);
            fputs(buf, stdout);
        }
    }
}
