#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>

#define MAX_CLIENTS 10
#define SERVER_PORT 31472
#define MAX_PENDING 5
#define MAX_LINE 256

int main()
{
    struct sockaddr_in sin;
    char buf[MAX_LINE];
    int len = sizeof(struct sockaddr_in);
    int s, new_s;

    /* build address data structure */
    bzero((char *)&sin, sizeof(sin));
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port = htons(SERVER_PORT);

    /* setup passive open */
    if ((s = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
        perror("simplex-talk: socket");
        exit(1);
    }

    if ((bind(s, (struct sockaddr *)&sin, sizeof(sin))) < 0) {
        perror("simplex-talk: bind");
        exit(1);
    }

    listen(s, MAX_PENDING);

    fd_set connset, queryset;
    FD_ZERO(&connset);
    FD_SET(s, &connset);
    int maxfd = s;

    // No client will ever hold the fd 0, as that's the stdin
    int clients[MAX_CLIENTS] = {0};
    struct sockaddr_in addresses[MAX_CLIENTS];

    /* wait for connection, then receive and print text */
    while(1) {
        queryset = connset;
        select(maxfd+1, &queryset, 0, 0, 0);
        if (FD_ISSET(s, &queryset)){
            printf("hey hey\n");
            if ((new_s = accept(s, (struct sockaddr *)&sin, &len)) < 0) {
                perror("simplex-talk: accept");
                exit(1);
            }
            int i;
            for (i = 0; i < MAX_CLIENTS; i++){
                if (!clients[i]){
                    clients[i] = new_s;
                    addresses[i] = sin;
                    FD_SET(new_s, &connset);
                    break;
                }
            }
            if (i == MAX_CLIENTS) {
                close(new_s);
                perror("Too many clients");
            }else{
                maxfd = maxfd > new_s ? maxfd : new_s;
            }
        }
        for (int i = 0; i < MAX_CLIENTS; i++){
            if (!clients[i]) continue;
            struct sockaddr_in *csock = &addresses[i];
            char *ip = (char *)&(csock->sin_addr.s_addr);
            int sock = clients[i];
            if (!FD_ISSET(sock, &queryset)) continue;
            if ((len = recv(sock, buf, sizeof(buf), 0)) > 0){
                printf("Printing information for %hhu.%hhu.%hhu.%hhu:%u\n",
                             ip[0], ip[1], ip[2], ip[3], ntohs(csock->sin_port));
                send(sock,buf,len,0);
                buf[len] = 0;
                fputs(buf, stdout);
            } else {
                FD_CLR(sock, &connset);
                close(sock);
                clients[i] = 0;
            }
        }
    }
    FD_CLR(s, &connset);
    close(s);
}
