#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <poll.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>

#define SERVER_PORT 31472
#define MAX_LINE 256

int main(int argc, char * argv[])
{
  FILE *fp;
  struct hostent *hp;
  struct sockaddr_in sin;
  char *host;
  char buf[MAX_LINE];
  int s;
  int len;
  if (argc==2) {
    host = argv[1];
  }
  else {
    fprintf(stderr, "usage: ./client host\n");
  exit(1);
}

  /* translate host name into peer’s IP address */
  hp = gethostbyname(host);
  if (!hp) {
    fprintf(stderr, "simplex-talk: unknown host: %s\n", host);
    exit(1);
  }

  /* build address data structure */
  bzero((char *)&sin, sizeof(sin));
  sin.sin_family = AF_INET;
  bcopy(hp->h_addr, (char *)&sin.sin_addr, hp->h_length);
  sin.sin_port = htons(SERVER_PORT);

  /* active open */
  if ((s = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
    perror("simplex-talk: socket");
    exit(1);
  }
  if (connect(s, (struct sockaddr *)&sin, sizeof(sin)) < 0) {
    perror("simplex-talk: connect");
    close(s);
    exit(1);
  }

  char recvbuf[MAX_LINE];
  int socklen = sizeof(struct sockaddr_in);
  struct sockaddr_in sout;
  getpeername(s, (struct sockaddr *)&sout, &socklen);
  char *a = (char *)&(sout.sin_addr.s_addr);
  printf("Printing information for %hhu.%hhu.%hhu.%hhu:%u\n",
         a[0], a[1], a[2], a[3], ntohs(sout.sin_port));
  /* main loop: get and send lines of text */
  while (fgets(buf, sizeof(buf), stdin)) {
    // see if we have data to show
    buf[MAX_LINE-1] = '\0';
    len = strlen(buf) + 1;
    send(s, buf, len, 0);
    recv(s, recvbuf, sizeof(recvbuf),0);
    printf("%s",recvbuf);
  }
  close(s);
}
